package pl.edu.pwsztar;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

//TODO: Podczas implementacji prosze pamietac o zasadzie F.I.R.S.T
public class ShoppingCartTest {
    ShoppingCart shoppingCart = new ShoppingCart();
    @ParameterizedTest
    @CsvSource({
            "Chocolate, 4, 4",
            "Milk, 10, 3",
            "Bread, 3, 4",
            "Juice, 3, 6"
    })

    void addProductToShoppingCartIsCorrect(String productName,int price, int amount){
        assertTrue(shoppingCart.addProducts(productName, price, amount));
    }

    @ParameterizedTest
    @CsvSource({
            "Chocolate, -10, 2",
            "Bread, 8, 0",
            "Milk, 5, -3",
            ",6,7",
            "Juice, 0, 3"
    })
    void addProductToShoppingCartIsNotCorrect(String productName, int price, int amount) {
        assertFalse(shoppingCart.addProducts(productName, price, amount));
    }

    @Test
    @DisplayName("Should Return Amount Of Products")
    void returnQuantityOfProductFromShoppingCart(String productName, int amount) {
        shoppingCart.addProducts("Chocolate", 10, 5);
        shoppingCart.addProducts("Milk", 4, 10);
        shoppingCart.addProducts("Juice", 5, 7);
        assertEquals(shoppingCart.getQuantityOfProduct(productName), amount);
    }

    @Test
    @DisplayName("Should Return Total Price All Products")
    void gettingSumPricesTest(){
        shoppingCart.addProducts("Chocolate",5,10);
        shoppingCart.addProducts("Juice",5,5);
        assertEquals(75,shoppingCart.getSumProductsPrices());
    }

    @ParameterizedTest
    @CsvSource({
            "Chocolate, 7",
            "Bread,  5",
            "Milk, 4",
            "Juice, 3"
    })
    void returnProductPriceFromShoppingCart(String productName, int price) {
        assertEquals(price, shoppingCart.getProductPrice(productName));
    }

    @Test
    @DisplayName("Should return List names of products")
    void getProductsNames() {
        final ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.addProducts("Chocolate", 10, 4);
        shoppingCart.addProducts("Milk", 8, 7);
        shoppingCart.addProducts("Juice", 2, 5);

        List<String> products = new ArrayList<>();
        products.add("chocolate");
        products.add("milk");
        products.add("juice");
        assertEquals(products, shoppingCart.getProductsNames());
    }

    @Test
     void test() {
        assertTrue(true);
    }
}
