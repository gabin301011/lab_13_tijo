package pl.edu.pwsztar;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ShoppingCart implements ShoppingCartOperation {
    List<Product> listOfProducts = new ArrayList<Product>();

    public boolean addProducts(String productName, int price, int quantity) {
        if(getSumAllProducts() + quantity > ShoppingCart.PRODUCTS_LIMIT || price <= 0 || quantity <= 0 || productName == null || productName.isEmpty()){
            return false;
        }
        productName = productName.toLowerCase();

        if(isProductAlreadyAdded(productName)){
            for(Product product: listOfProducts){
                if(product.getProductName().equals(productName) && product.getPrice() != price){
                    product.setQuantity(product.getQuantity() + quantity);
                    return true;
                }
            }
        }else{
            listOfProducts.add(new Product(productName, price, quantity));
            return true;
        }
        return false;
    }

    public boolean deleteProducts(String productName, int quantity) {
        return false;
    }

    public int getQuantityOfProduct(String productName) {
        for (Product x: listOfProducts){
            if(x.getProductName().equals(productName)){
                return x.getQuantity();
            }
        }
        return 0;
    }

    public int getSumProductsPrices() {
        int sum=0;
        for(Product x: listOfProducts) {
            sum += (x.getPrice()*x.getQuantity());
        }
        return sum;
    }

    public int getProductPrice(String productName) {
        if(productName.equals("")){
            return 0;
        }
        for(Product x: listOfProducts){
            if(x.getProductName().equals(productName)){
                return x.getPrice();
            }
        }
        return 0;
    }


    public List<String> getProductsNames() {
        List<String> result = new ArrayList<>();
        for(Product product: listOfProducts)
        {
            result.add(product.getProductName());
        }
        return result;
    }

    private boolean isProductAlreadyAdded(String name){
        return listOfProducts.stream()
                .anyMatch(product -> product.getProductName()
                        .equals(name));
    }


    private  int getSumAllProducts(){
        return listOfProducts.stream()
                .mapToInt(product -> product.getPrice() * product.getQuantity())
                .sum();
    }
}
